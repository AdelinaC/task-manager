### Task Manager ###

Task Manager application - responsible for handling multiple processes

##### BUILD & RUN #####
In the main folder of the project, run the following:

- BUILD: `gradle clean build`
- TEST: `gradle test` 
- RUN: `java -jar ./build/libs/TaskManager-1.0-SNAPSHOT.jar <default|fifo|priority> <capacity>`

e.g. `java -jar ./build/libs/TaskManager-1.0-SNAPSHOT.jar fifo 2`

##### Versions #####

- `Gradle: 6.6.1`
- `OpenJDK: 11.0.8`
- `JVM: 4.0.1 (Oracle Corporation 14.0.1+14)`
- `OS: Mac OS X 10.16 x86_64`

##### Technical Considerations #####

###### Language #####
The implementation has been done in Java because of the experience that I have with this programming language.
A migration to Kotlin should be quite easy, but due to the time constraints, I preferred to stay with Java for now. 

###### Extensibility ######
The functionality can be easily extended by adding another TaskManager strategy and overriding the needed methods 
in order to provide the required functionality.

The TaskManager is using a GenericProcessQueue to store the processes handled by it.

##### Supported operations #####

- add <PID> <priority>
- list               
- kill <PID>         
- killGroup <priority>
- killAll             
- exit               
- help                


##### Improvements #####

- Due to time constraints, the entire functionality has been tested only using integration tests. 
The classes haven't been tested individually, but through the *TaskManagerApplicationTest* integration test.
- Logging should be defined
- The application can accept only input from stdin
- Exception handling
- Validation for all the operations not in place






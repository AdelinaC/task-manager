package achelcea;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static achelcea.util.StringUtils.DEFAULT_SPACES;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TaskManagerApplicationTest {

    private static String HEADER = "PID" + DEFAULT_SPACES + "Priority\n";

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidNumberOfParametersCall() {
        TaskManagerApplication.main(new String[]{"Invalid number of parameters"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidTaskManagerTypeCall() {
        TaskManagerApplication.main(new String[]{"Undefined", "2"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidTaskManagerCapacityCall() {
        TaskManagerApplication.main(new String[]{"FIFO", "two"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidTaskManagerCapacityTooSmallCall() {
        TaskManagerApplication.main(new String[]{"FIFO", "0"});
    }
    @Test
    public void testInvalidArgumentsForAddingProcess(){
        String userInput = String.format("add 1 low\n" +
                "add 2       \n" +
                "add 3     low\n" +
                "list\n" +
                "exit\n");
        ByteArrayInputStream bais = new ByteArrayInputStream(userInput.getBytes());
        System.setIn(bais);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(baos);
        System.setOut(printStream);

        String expected = "1" + DEFAULT_SPACES + "LOW" + "\n" +
                "3" + DEFAULT_SPACES + "LOW";
        TaskManagerApplication.main(new String[]{"FIFO","2"});

        assertTrue(baos.toString().contains("Process could not be created. Please provide valid arguments"));
        assertTrue(baos.toString().contains(HEADER + expected));
    }

    @Test
    public void testFifoAdd_ShouldResultInExpectedOutput(){
        String userInput = String.format("add 1 low\n" +
                "add 2       low\n" +
                "add 3     low\n" +
                "list\n" +
                "exit\n");
        ByteArrayInputStream bais = new ByteArrayInputStream(userInput.getBytes());
        System.setIn(bais);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(baos);
        System.setOut(printStream);

        String expected = "2" + DEFAULT_SPACES + "LOW" + "\n" +
                "3" + DEFAULT_SPACES + "LOW";
        TaskManagerApplication.main(new String[]{"FIFO","2"});

        assertTrue(baos.toString().contains(HEADER + expected));
    }

    @Test
    public void testDefaultAddExceedingCapacity() {
        String userInput = String.format("add 1 low\n" +
                "add 2 low\n" +
                "add 3 low\n" +
                "list\n" +
                "exit");
        ByteArrayInputStream bais = new ByteArrayInputStream(userInput.getBytes());
        System.setIn(bais);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(baos);
        System.setOut(printStream);

        String expected = "1" + DEFAULT_SPACES + "LOW" + "\n" +
                "2" + DEFAULT_SPACES + "LOW";

        TaskManagerApplication.main(new String[]{"DEFAULT","2"});
        assertTrue(baos.toString().contains("No space left for the current process.\n" +
                "Please remove from existing ones to insert other processes"));
        assertTrue(baos.toString().contains(HEADER + expected));
    }

    @Test
    public void testPriorityTaskManagerAdd_ShouldResultInExpectedOutput() {
        String userInput = String.format("add 1 high\n" +
                "add 2 low\n" +
                "add 3 low\n" +
                "add 4 high\n" +
                "list\n" +
                "exit\n");
        ByteArrayInputStream bais = new ByteArrayInputStream(userInput.getBytes());
        System.setIn(bais);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(baos);
        System.setOut(printStream);

        String expected = "1" + DEFAULT_SPACES + "HIGH" + "\n" +
                "3" + DEFAULT_SPACES + "LOW"+ "\n" +
                "4" + DEFAULT_SPACES + "HIGH";
        TaskManagerApplication.main(new String[]{"PRIORITY","3"});

        assertTrue(baos.toString().contains(HEADER + expected));
    }

    @Test
    public void testDefaultTaskManagerAddDuplicateProcess_ShouldResultInExpectedOutput() {
        String userInput = String.format("add 1 high\nadd 1 low\nlist\nexit");
        ByteArrayInputStream bais = new ByteArrayInputStream(userInput.getBytes());
        System.setIn(bais);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(baos);
        System.setOut(printStream);

        String expected = "1" + DEFAULT_SPACES + "HIGH";
        String notExpected = "1" + DEFAULT_SPACES + "LOW";
        TaskManagerApplication.main(new String[]{"DEFAULT","3"});

        assertTrue(baos.toString().contains(HEADER + expected));
        assertFalse(baos.toString().contains(notExpected));
    }

    @Test
    public void testDefaultTaskManagerKillProcesses_ShouldResultInExpectedOutput() {
        String userInput = String.format("add 1 high\n" +
                "add 2 low\n" +
                "add 3 low\n" +
                "list\n" +
                "kill 1\n" +
                "list\n" +
                "add 4 high\n" +
                "list\n" +
                "killGroup low\n" +
                "list\n" +
                "exit");
        ByteArrayInputStream bais = new ByteArrayInputStream(userInput.getBytes());
        System.setIn(bais);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(baos);
        System.setOut(printStream);

        String expectedAfterFirstAdd = "1" + DEFAULT_SPACES + "HIGH" + "\n" +
                "2" + DEFAULT_SPACES + "LOW" + "\n" +
                "3" + DEFAULT_SPACES + "LOW";
        String expectedAfterKill1 = "2" + DEFAULT_SPACES + "LOW" + "\n" +
                "3" + DEFAULT_SPACES + "LOW";
        String expectedAfterAdd4 = "2" + DEFAULT_SPACES + "LOW" + "\n" +
                "3" + DEFAULT_SPACES + "LOW" + "\n" +
                "4" + DEFAULT_SPACES + "HIGH";
        String expectedAfterKillGroupLow = "4" + DEFAULT_SPACES + "HIGH";

        TaskManagerApplication.main(new String[]{"DEFAULT","3"});

        assertTrue(baos.toString().contains(HEADER + expectedAfterFirstAdd));
        assertTrue(baos.toString().contains(HEADER + expectedAfterKill1));
        assertTrue(baos.toString().contains(HEADER + expectedAfterAdd4));
        assertTrue(baos.toString().contains(HEADER + expectedAfterKillGroupLow));
    }

    @Test
    public void testFifoTaskManagerKillProcesses_ShouldResultInExpectedOutput() {
        String userInput = String.format("add 1 high\n" +
                "add 2 low\n" +
                "add 3 low\n" +
                "add 4 medium\n" +
                "list\n" +
                "kill 4\n" +
                "list\n" +
                "add 5 high\n" +
                "list\n" +
                "killGroup low\n" +
                "list\n" +
                "exit");
        ByteArrayInputStream bais = new ByteArrayInputStream(userInput.getBytes());
        System.setIn(bais);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(baos);
        System.setOut(printStream);

        String expectedAfterFirstAdd = "2" + DEFAULT_SPACES + "LOW" + "\n" +
                "3" + DEFAULT_SPACES + "LOW" + "\n" +
                "4" + DEFAULT_SPACES + "MEDIUM";
        String expectedAfterKill1 = "2" + DEFAULT_SPACES + "LOW" + "\n" +
                "3" + DEFAULT_SPACES + "LOW";
        String expectedAfterAdd5 = "2" + DEFAULT_SPACES + "LOW" + "\n" +
                "3" + DEFAULT_SPACES + "LOW" + "\n" +
                "5" + DEFAULT_SPACES + "HIGH";
        String expectedAfterKillGroupLow = "5" + DEFAULT_SPACES + "HIGH";

        TaskManagerApplication.main(new String[]{"FIFO","3"});

        assertTrue(baos.toString().contains(HEADER + expectedAfterFirstAdd));
        assertTrue(baos.toString().contains(HEADER + expectedAfterKill1));
        assertTrue(baos.toString().contains(HEADER + expectedAfterAdd5));
        assertTrue(baos.toString().contains(HEADER + expectedAfterKillGroupLow));
    }

    @Test
    public void testPriorityTaskManagerKillProcesses_ShouldResultInExpectedOutput() {
        String userInput = String.format("add 1 high\n" +
                "add 2 low\n" +
                "add 3 low\n" +
                "add 4 medium\n" +
                "list\n" +
                "kill 4\n" +
                "list\n" +
                "add 5 high\n" +
                "list\n" +
                "killGroup low\n" +
                "list\n" +
                "exit");
        ByteArrayInputStream bais = new ByteArrayInputStream(userInput.getBytes());
        System.setIn(bais);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(baos);
        System.setOut(printStream);

        String expectedAfterFirstAdd = "1" + DEFAULT_SPACES + "HIGH" + "\n" +
                "3" + DEFAULT_SPACES + "LOW" + "\n" +
                "4" + DEFAULT_SPACES + "MEDIUM";
        String expectedAfterKill1 = "1" + DEFAULT_SPACES + "HIGH" + "\n" +
                "3" + DEFAULT_SPACES + "LOW";
        String expectedAfterAdd5 = "1" + DEFAULT_SPACES + "HIGH" + "\n" +
                "3" + DEFAULT_SPACES + "LOW" + "\n" +
                "5" + DEFAULT_SPACES + "HIGH";
        String expectedAfterKillGroupLow = "1" + DEFAULT_SPACES + "HIGH" + "\n" +
                "5" + DEFAULT_SPACES + "HIGH";

        TaskManagerApplication.main(new String[]{"PRIORITY","3"});

        assertTrue(baos.toString().contains(HEADER + expectedAfterFirstAdd));
        assertTrue(baos.toString().contains(HEADER + expectedAfterKill1));
        assertTrue(baos.toString().contains(HEADER + expectedAfterAdd5));
        assertTrue(baos.toString().contains(HEADER + expectedAfterKillGroupLow));
    }

    @Test
    public void testPriorityTaskManagerKillAllProcess_ShouldResultInExpectedOutput() {
        String userInput = String.format("add 1 high\n" +
                "add 2 low\n" +
                "add 3 low\n" +
                "add 4 medium\n" +
                "killAll\n" +
                "list\n" +
                "exit");
        ByteArrayInputStream bais = new ByteArrayInputStream(userInput.getBytes());
        System.setIn(bais);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(baos);
        System.setOut(printStream);

        String notExpectedAfterKillAll = "1" + DEFAULT_SPACES + "HIGH" + "\n" +
                "3" + DEFAULT_SPACES + "LOW" + "\n" +
                "4" + DEFAULT_SPACES + "MEDIUM";

        TaskManagerApplication.main(new String[]{"PRIORITY","3"});

        assertFalse(baos.toString().contains(notExpectedAfterKillAll));

    }
}

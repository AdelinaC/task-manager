package achelcea.taskmanager;

import achelcea.process.GenericProcessQueue;
import achelcea.process.Process;

public class DefaultTaskManager extends TaskManager {

    public DefaultTaskManager(GenericProcessQueue processQueue) {
        super(processQueue);
    }

    public boolean add(Process process) {
        GenericProcessQueue processQueue = this.getProcessQueue();
        if (processQueue.isFull()) {
            System.out.println("No space left for the current process.\n" +
                    "Please remove from existing ones to insert other processes");
            return false;
        }
        addProcessToPriorityMap(process);
        return processQueue.add(process);
    }
}

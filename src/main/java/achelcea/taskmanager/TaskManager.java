package achelcea.taskmanager;

import achelcea.process.GenericProcessQueue;
import achelcea.process.Priority;
import achelcea.process.Process;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class TaskManager {

    private GenericProcessQueue processQueue;
    private Map<Priority, List<Process>> priorityMap;

    public TaskManager(GenericProcessQueue processQueue) {
        this.processQueue = processQueue;
        this.priorityMap = new HashMap<>();
    }

    public GenericProcessQueue getProcessQueue() {
        return processQueue;
    }

    protected Map<Priority, List<Process>> getPriorityMap() {
        return priorityMap;
    }

    public abstract boolean add(Process process);

    public List<Process> getRunningProcesses() {
        return processQueue.getRunningProcesses();
    }

    public void kill(int PID) {
        Process crtProcess = processQueue.removeById(PID);
        if (crtProcess != null) {
            priorityMap.get(crtProcess.getPriority()).remove(crtProcess);
        }
    }

    public void killGroup(Priority priority) {
        List<Process> processesWithPriority = priorityMap.get(priority);
        while (processesWithPriority != null && !processesWithPriority.isEmpty()) {
            kill(processesWithPriority.get(0).getPID());
        }
        priorityMap.remove(priority);
    }

    public void killAll() {
        for (Priority priority : Priority.values()) {
            killGroup(priority);
        }
    }

    protected void addProcessToPriorityMap(Process process) {
        priorityMap.putIfAbsent(process.getPriority(), new LinkedList<>());
        priorityMap.get(process.getPriority()).add(process);
    }
}

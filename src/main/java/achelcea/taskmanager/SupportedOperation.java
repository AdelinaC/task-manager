package achelcea.taskmanager;

public enum SupportedOperation {
    EXIT,
    HELP,
    ADD,
    LIST,
    KILL,
    KILLGROUP,
    KILLALL
}

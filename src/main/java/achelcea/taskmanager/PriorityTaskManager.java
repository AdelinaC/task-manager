package achelcea.taskmanager;

import achelcea.process.Priority;
import achelcea.process.Process;
import achelcea.process.GenericProcessQueue;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class PriorityTaskManager extends TaskManager {

    public PriorityTaskManager(GenericProcessQueue processQueue) {
        super(processQueue);
    }

    @Override
    public boolean add(Process process) {
        GenericProcessQueue processQueue = this.getProcessQueue();
        if (processQueue.isFull()) {
            Priority lowestPriority = getLowestAvailablePriorityThanCurrent(process.getPriority(),
                    this.getPriorityMap());
            if (lowestPriority == null) {
                System.out.println("no process was removed");
                return false;
            }
            Process toBeRemoved = this.getPriorityMap().get(lowestPriority).remove(0);
            kill(toBeRemoved.getPID());
        }
        processQueue.add(process);
        addProcessToPriorityMap(process);
        return true;
    }

    private Priority getLowestAvailablePriorityThanCurrent(Priority priority, Map<Priority, List<Process>> priorityMap) {
        if (!priorityMap.getOrDefault(Priority.LOW, new LinkedList<>()).isEmpty()) {
            return Priority.LOW;
        }
        if (!priorityMap.getOrDefault(Priority.MEDIUM, new LinkedList<>()).isEmpty() &&
                priority != Priority.LOW) {
            return Priority.MEDIUM;
        }
        if (!priorityMap.getOrDefault(Priority.HIGH, new LinkedList<>()).isEmpty() &&
                priority != Priority.LOW && priority != Priority.MEDIUM) {
            return Priority.HIGH;
        }
        return null;
    }
}

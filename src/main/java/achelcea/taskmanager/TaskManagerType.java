package achelcea.taskmanager;

public enum TaskManagerType {
    DEFAULT,
    FIFO,
    PRIORITY
}

package achelcea.taskmanager;

import achelcea.process.GenericProcessQueue;

public class TaskManagerFactory {

    public static TaskManager buildTaskManager(TaskManagerType taskManagerType, GenericProcessQueue genericProcessQueue) {
        switch (taskManagerType) {
            case FIFO: return new FifoTaskManager(genericProcessQueue);
            case PRIORITY: return new PriorityTaskManager(genericProcessQueue);
            default: return new DefaultTaskManager(genericProcessQueue);
        }
    }
}

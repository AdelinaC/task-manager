package achelcea.taskmanager;

import achelcea.process.Process;
import achelcea.process.GenericProcessQueue;

public class FifoTaskManager extends TaskManager {

    public FifoTaskManager(GenericProcessQueue processQueue) {
        super(processQueue);
    }

    @Override
    public boolean add(Process process) {
        GenericProcessQueue processQueue = getProcessQueue();
        if (processQueue.isFull()) {
            Process toBeRemovedProcess = processQueue.getRunningProcesses().get(0);
            kill(toBeRemovedProcess.getPID());
        }
        addProcessToPriorityMap(process);
        return processQueue.add(process);
    }
}

package achelcea.util;

import java.util.regex.Pattern;

public class StringUtils {

    public static String DEFAULT_SPACES = "     ";

    private static Pattern PATTERN = Pattern.compile("-?\\d+(\\.\\d+)?");

    public static String sanitizeString(String readLine) {
        return readLine.replaceAll("^ +| +$|( )+", "$1");
    }

    public static Integer getNumberFromString(String numberAsString) {
        if (PATTERN.matcher(numberAsString).matches()) {
            return Integer.parseInt(numberAsString);
        }
        return null;
    }

}

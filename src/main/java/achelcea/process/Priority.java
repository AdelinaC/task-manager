package achelcea.process;

public enum Priority {

    LOW,
    MEDIUM,
    HIGH
}

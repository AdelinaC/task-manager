package achelcea.process;

import achelcea.util.StringUtils;

public final class Process {

    public int getPID() {
        return PID;
    }

    private int PID;

    public Priority getPriority() {
        return priority;
    }

    private Priority priority;

    public Process(int PID, Priority priority) {
        this.PID = PID;
        this.priority = priority;
    }

    @Override
    public String toString() {
        return this.PID + StringUtils.DEFAULT_SPACES + this.priority;

    }
}

package achelcea.process;

import java.util.*;

public class GenericProcessQueue {

    private int capacity;
    private List<Process> processQueue;
    private Map<Integer, Process> existingProcesses;

    public GenericProcessQueue(int capacity) {
        this.capacity = capacity;
        this.processQueue = new LinkedList<>();
        this.existingProcesses = new HashMap<>();
    }

    public boolean isFull() {
        return processQueue.size() == capacity;
    }

    public boolean add(Process process) {
        if (existingProcesses.keySet().contains(process.getPID())) {
            return false;
        }

        processQueue.add(process);
        existingProcesses.put(process.getPID(), process);
        return true;
    }

    public Process removeById(int PID) {
        Process crtProcess = existingProcesses.remove(PID);
        processQueue.remove(crtProcess);
        return crtProcess;
    }

    public List<Process> getRunningProcesses() {
        return processQueue;
    }

}

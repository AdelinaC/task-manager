package achelcea;

import achelcea.process.GenericProcessQueue;
import achelcea.process.Priority;
import achelcea.process.Process;
import achelcea.taskmanager.SupportedOperation;
import achelcea.taskmanager.TaskManager;
import achelcea.taskmanager.TaskManagerFactory;
import achelcea.taskmanager.TaskManagerType;
import org.apache.commons.lang3.EnumUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.regex.Pattern;

import static achelcea.util.StringUtils.*;

public class TaskManagerApplication {

    private static Pattern PATTERN = Pattern.compile("-?\\d+(\\.\\d+)?");

    public static void main(String[] args) {
        if (args.length != 2) {
            throw new IllegalArgumentException("The Task Manager strategy and its capacity must be provided at built time");
        }

        TaskManagerType type = getTaskManagerType(args[0].toUpperCase());
        int capacity = getTaskManagerCapacity(args[1]);

        System.out.println("Current configuration: " + type.name() + " " + capacity);

        GenericProcessQueue processQueue = new GenericProcessQueue(capacity);
        TaskManager taskManager = TaskManagerFactory.buildTaskManager(type, processQueue);

        listAvailableCommands();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                System.out.print("> ");
                String command = sanitizeString(reader.readLine());
                int result = executeCommand(command, taskManager);
                if (result == -1) {
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static int getTaskManagerCapacity(String capacityAsString) {
        Integer capacity = getNumberFromString(capacityAsString);
        if (capacity == null || capacity < 1) {
              throw new IllegalArgumentException("The capacity for the Task Manager must be a positive integer number bigger than 0");
        }
        return capacity;
    }

    private static TaskManagerType getTaskManagerType(String taskManagerType) {
        if (EnumUtils.isValidEnum(TaskManagerType.class, taskManagerType)) {
            return TaskManagerType.valueOf(taskManagerType);
        } else {
            throw new IllegalArgumentException("TaskManagerType must be <default|fifo|priority>");
        }
    }

    private static int executeCommand(String command, TaskManager taskManager) {
        String[] tokenized = command.split(" ");
        String operation = tokenized[0];
        String[] args = Arrays.copyOfRange(tokenized, 1, tokenized.length);

        if (EnumUtils.isValidEnum(SupportedOperation.class, operation.toUpperCase())) {
            SupportedOperation supportedOperation = SupportedOperation.valueOf(operation.toUpperCase());

            switch (supportedOperation) {
                case EXIT: return -1;
                case HELP: {
                    listAvailableCommands();
                    break;
                }
                case ADD: {
                    addProcess(taskManager, args);
                    break;
                }
                case LIST: {
                    listRunningProcesses(taskManager);
                    break;
                }
                case KILL: {
                    killCurrentProcess(taskManager, args[0]);
                    break;
                }
                case KILLGROUP: {
                    removeByPriority(taskManager, args);
                    break;
                }
                case KILLALL: {
                    taskManager.killAll();
                    break;
                }
            }
        } else {
            System.out.print("Please type help for listing available functionalities > ");
        }
        return 0;
    }

    private static void killCurrentProcess(TaskManager taskManager, String processIdAsString) {
        Integer processId = getNumberFromString(processIdAsString);
        if (processId != null) {
            taskManager.kill(processId);
        } else {
            System.out.println("The PID must be a number");
        }
    }

    private static void listRunningProcesses(TaskManager taskManager) {
        System.out.println("PID" + DEFAULT_SPACES + "Priority");
        taskManager.getRunningProcesses().forEach(System.out::println);
    }

    private static void removeByPriority(TaskManager taskManager, String[] args) {
        Priority priority = getPriorityToBeRemoved(args);
        taskManager.killGroup(priority);
    }

    private static void addProcess(TaskManager taskManager, String[] args) {
        Process process = createProcess(args);
        if (process != null) {
            taskManager.add(process);
        }
    }

    private static Priority getPriorityToBeRemoved(String[] args) {
        Priority crtPriority = Priority.valueOf(args[0].toUpperCase());
        return crtPriority;
    }

    private static Process createProcess(String[] tokenized) {
        if (tokenized.length < 2) {
            System.out.println("Process could not be created. Please provide valid arguments");
            return null;
        }
        int pid = Integer.parseInt(tokenized[0]);
        if (EnumUtils.isValidEnum(Priority.class, tokenized[1].toUpperCase())) {
            Priority priority = Priority.valueOf(tokenized[1].toUpperCase());
            return new Process(pid, priority);
        } else {
            System.out.println(tokenized[1] + "is not a valid priority value\n" +
                    "Please use low|medium|high");
            return null;
        }
    }

    private static void listAvailableCommands() {
        System.out.println("Please type one of the following:");
        System.out.println("-> add <PID> <priority>");
        System.out.println("-> list");
        System.out.println("-> kill <PID>");
        System.out.println("-> killGroup <priority>");
        System.out.println("-> killAll");
        System.out.println("-> exit");
        System.out.println("-> help");
    }
}
